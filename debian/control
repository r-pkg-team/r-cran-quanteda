Source: r-cran-quanteda
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Joost van Baal-Ilić <joostvb@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper (>= 11~),
               dh-r,
               r-base-dev,
               r-cran-data.table (>= 1.9.6),
               r-cran-fastmatch,
               r-cran-ggplot2 (>= 2.2.0),
               r-cran-lubridate,
               r-cran-magrittr,
               r-cran-matrix (>= 1.2),
               r-cran-rspectra,
               r-cran-rcpp (>= 0.12.12),
               r-cran-snowballc,
               r-cran-stringi,
               r-cran-xml2,
               r-cran-yaml,
               r-cran-rcpparmadillo (>= 0.7.600.1.0)
Standards-Version: 4.0.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-quanteda
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-quanteda.git
Homepage: https://cran.r-project.org/package=quanteda

Package: r-cran-quanteda
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Quantitative Analysis of Textual Data
 A fast, flexible, and comprehensive framework for quantitative text analysis
 in R.  Provides functionality for corpus management, creating and manipulating
 tokens and ngrams, exploring keywords in context, forming and manipulating
 sparse matrices of documents by features and feature co-occurrences, analyzing
 keywords, computing feature similarities and distances, applying content
 dictionaries, applying supervised and unsupervised machine learning, visually
 representing text and text analyses, and more.
